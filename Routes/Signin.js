const express = require('express');
const Signin = express.Router();
const LoginModal = require('../Modal/LoginSchema');

Signin.post('/', async (req, res) => {
  const { Username, Password } = req.body;
  try {
    const user = await LoginModal.findOne({ "Username": Username });

    if (user) {
      if (user.Password.toString() === Password.toString()) {
        res.send(user);
      } else {
        // Password doesn't match, send password incorrect message
        res.status(400).send("Password incorrect");
      }
    } else {
      // User doesn't exist, send user not found message
      res.status(400).send("User not found. Please register.");
    }
  } catch (error) {
    console.error(error);
    res.status(500).send("Server error");
  }
});

module.exports = Signin;


