const express = require('express');
const Signup = express.Router();
const SignupModal = require('../Modal/SignupSchema');

Signup.post('/', async (req, res) => {
  try {
    const data = {
      "Username": req.body.Username,
      "mail": req.body.mail,
      "Password": req.body.Password
    };
    const result = await SignupModal.create(data);
    res.send("Data successfully inserted: " + result);
  } catch (error) {
    console.error(error);
    res.status(500).send("Server error");
  }
});

module.exports = Signup;
