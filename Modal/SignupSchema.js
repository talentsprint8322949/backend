const mongoose = require('mongoose');

const SignupSchema = new mongoose.Schema({
  Username: {
    type: String,
    required: true,
  },
  mail: {
    type: String,
    required: true,
  },
  Password: {
    type: String,
    required: true,
  },
});

const SignupModal = mongoose.model('signups', SignupSchema);
module.exports = SignupModal;
