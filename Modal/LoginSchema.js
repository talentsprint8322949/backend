const mongoose = require('mongoose');

if (!mongoose.models['signups']) {
  const LoginSchema = new mongoose.Schema({
    Username: {
      type: String,
      required: true,
    },
    Password: {
      type: String,
      required: true,
    },
  });

  mongoose.model('signups', LoginSchema);
}

const LoginModal = mongoose.model('signups');
module.exports = LoginModal;
